#This is the main script for news aggregator algorithm/heuristic
import datetime
import pickle
import random
finaldict=pickle.load(open('date_to_const_count.p','rb'))
def get_desired_dist():
	#For now, it is manually initialized. Later, we will revert the click
	#coordinate on PCA plot to generate it.
	dd=[0.5,0.3,0.1,0.05,0.05]
	return dd

def get_production_dist(startdate,enddate):
	const_count=[0,0,0,0,0]
	for date in finaldict.keys():
		if date>=startdate and date<=enddate:
			for i in range(0,5):
				const_count[i]+=finaldict[date][i]
	print("Production distribution calculated.")
	return(const_count)

def get_achieved_dist(startdate,enddate):
	const_count=[0,0,0,0,0]
	for date in finaldict.keys():
		if date>=startdate and date<=enddate:
			for i in range(0,5):
				const_count[i]+=finaldict[date][i]
	print("Production distribution calculated.")
	return(const_count)

def get_short_term_dist(end):
	#Generally, the spikes disappear in a 12 days time period (need to check)
	#This has been observed from the moving average plot
	startdate=end-timedelta(days=12)
	enddate=end
	for date in finaldict.keys():
		if date>=startdate and date<=enddate:
			for i in range(0,5):
				const_count[i]+=finaldict[date][i]
	print("Production distribution calculated.")
	return(const_count)

def calculate_score(P,A,D,SP,eta=0.3):
	s=[]
	n[j]=P[j]
	for j in range(0,5):
		s.append((eta*(1/float(n[j]))*abs(D[j] - A[j])) + ((1-eta)*abs(SP[j] - P[j]))):wq
	return s

if __name__ == '__main__':
	
	print("For the production function,")
	start=input("Enter the start date (YYYY/MM/DD):")
	print type(start)
	exit(0)
	end=input("Enter the end date (YYYY/MM/DD):")

	start_year=start.split('/')[0]
	start_month=start.split('/')[1]
	start_day=start.split('/')[2]

	end_year=end.split('/')[0]
	end_month=end.split('/')[1]
	end_day=end.split('/')[2]

	start=datetime.date(start_year,start_month,start_day)
	end=datetime.date(end_year,end_month,end_day)

	#Right now, for achieved distribution we are taking a time delta of 5 days.
	#Later, it needs to be adjusted from Monday of every week. So, the time
	#delta will vary from 0 to 7 depending on the day of the week for the end.
	start_achieved= end - timedelta(days=5)

	P=get_production_dist(start,end)
	A=get_achieved_dist(start_achieved,end)
	D=get_desired_dist()
	SP=get_short_term_dist(end)
	scores=calculate_score(P,A,D,SP,eta)
	print(scores)

	
	date_to_url=pickle.load(open('date_to_url.p','rb'))
	date_to_aid=pickle.load(open('date_to_aid.p','rb'))
	aid_to_const=pickle.load(open('aid_to_const.p','rb'))
	#allot_articles(date_to aid_to_const)

	#print len(date_to_url[datetime.date(2017, 5, 22)])
	#print('\n')
	#print len(date_to_aid[datetime.date(2017, 5, 22)])
	#for score in sorted(scores,reverse=True):
	#	ind=scores.index(score)


