#Script to read tables from postgresql in gem2 server
import psycopg2
from collections import defaultdict
import matplotlib.pyplot as plt
from datetime import datetime,timedelta,date
from operator import add
import datetime
import pandas as pd
from statsmodels.tsa.stattools import adfuller
import pickle
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima_model import ARIMA
import matplotlib.pyplot as plt
import numpy
#from pmdarima.arima.utils import ndiffs
def create_aspect_to_const_mapping():
	fin=open('demon_aspects.csv','r')
	aid_to_aname=dict()
	aid_to_const=dict()
	c=0
	for line in fin:
		c+=1
		if c==1:
			continue
		l=line.rstrip().split(',')
		aid=l[0]
		aname=l[1]
		const_l=[]
		
		for j in range(2,7):
			if int(l[j])!=0:
				const_l.append(j)
		
		aid_to_const[aid]=const_l
		aid_to_aname[aid]=aname
	fin.close()
	pickle.dump(aid_to_const,open('aid_to_const.p','wb'))
	return(aid_to_aname,aid_to_const)	
def plot_histogram(date_to_cid):
	x=[2,3,4,5,6]
	#Sort the dict on dates
	sorted_dates=sorted(date_to_cid.keys())
	sorted_dict=dict()
	for date in sorted_dates:
		sorted_dict[date]=date_to_cid[date]
	#Final dict to store sum of counts of const ids for multiple dates (each key is a bin)	
	finaldict=dict()

	for i in range(0,len(sorted_dict.keys()),7):
		result=[0,0,0,0,0]
		for j in range(i,i+7):
			if j>len(sorted_dates)-1:
				break
			else:
				result[0]+=sorted_dict[sorted_dates[j]].count(2)
				result[1]+=sorted_dict[sorted_dates[j]].count(3)
				result[2]+=sorted_dict[sorted_dates[j]].count(4)
				result[3]+=sorted_dict[sorted_dates[j]].count(5)
				result[4]+=sorted_dict[sorted_dates[j]].count(6)
		
		#print(sorted_dates[i+6])
		#print(sorted_dict[sorted_dates[i+6]])
		#print (sorted_dict[sorted_dates[i]],sorted_dict[sorted_dates[i+6]])
		if j<len(sorted_dates):
			finaldict[(sorted_dates[i],sorted_dates[i+6])]=result
	print(finaldict)
	
	#Plot the graphs for each date bin
	for tup in finaldict.keys():
		x=[0,1,2,3,4]
		y=finaldict[tup]
		#plt.plot(x,y)
		plt.bar(x,y)
		#print(x)
		#print(y)
		plt.savefig('./plots/demon_const_cov.'+str(tup)+'.png')
		plt.close()
		
def get_date_wise_const_count():
	'''
	#sort the dict on dates
	sorted_dates=sorted(date_to_cid.keys())
	sorted_dict=dict()
	for date in sorted_dates:
		sorted_dict[date]=date_to_cid[date]
	#Final dict to store sum of counts of const ids for a single date	
	finaldict=dict()
		#For every date, store the count of const.
	for i in sorted_dict.keys():
		result=[0,0,0,0,0]
			for j in range(0,5):
		result[j]=sorted_dict[i].count(j+2)
		finaldict[i]=result
		pickle.dump(finaldict,open("date_to_const_count.p",'wb'))
	'''
	finaldict=pickle.load(open('date_to_const_count.p','rb'))
	return finaldict
def get_urls(date_to_url):
	start=datetime.date(2017,1,31)
	end=datetime.date(2017,2,6)
	for date in date_to_url.keys():
		if date>=start and date<=end:
			print(str(date)+'\t'+';'.join(date_to_url[date]))

def detect_anomaly(df,window_size):
	#Check if the timeseries is stationary (constant mean, s.d., etc.)
	print (df)
	#df=(df[df.columns[0]])
	result = adfuller(df)
	print('ADF Statistic: %f' % result[0])
	print('p-value: %f' % result[1])

	'''
	#Check the p value in ARIMA
	plt.rcParams.update({'figure.figsize':(9,3), 'figure.dpi':120})

	fig, axes = plt.subplots(1, 2, sharex=True)
	axes[0].plot(df.diff()); axes[0].set_title('1st Differencing')
	axes[1].set(ylim=(0,5))
	plot_pacf(df.diff().dropna(), ax=axes[1])

	plt.show()
	'''

	'''
	#ARIMA Model
	for i in range(1,6):
		for j in range(1,6):
			model = ARIMA(df, order=(0,i,j))
			model_fit = model.fit(disp=0)
			print(model_fit.summary())
	#print(df)
	'''
	window= numpy.ones(int(window_size))/float(window_size)
	return numpy.convolve(df, window, 'same')


aid_to_aname,aid_to_const=create_aspect_to_const_mapping()


connection = psycopg2.connect(user="deb",
							  password="Deb@12345",
							  host="10.237.26.159",
							  port="5432",
							  database="media_database")
cursor = connection.cursor()
postgreSQL_select_Query = "select * from art_info_final where event_id=1"
cursor.execute(postgreSQL_select_Query)

records = cursor.fetchall() 
fout=open('demon_details.csv','w')
print("Print each row and it's columns values")
date_to_aid=defaultdict(list)
date_to_url=defaultdict(list)
for row in records:
	aspect_id=row[2]
	date=row[13]
	url=row[4]
	date_to_aid[date].extend(aspect_id)
	date_to_url[date].append(url)
pickle.dump(date_to_url,open('date_to_url.p','wb'))
pickle.dump(date_to_aid,open('date_to_aid.p','wb'))
exit(0)	

#convert aspect IDs to constituency IDs for each date
date_to_cid=defaultdict(list)
for date in date_to_aid.keys():
	aid_l=date_to_aid[date]
	for aid in aid_l:
		#Some aid's are present in postgres but not in the mapping sheet (ask Debanjan)
		try:
			const=aid_to_const[aid]
			date_to_cid[date].extend(const)
		except:
			continue
		
'''
date_to_const_count=get_date_wise_const_count()
df=pd.DataFrame.from_dict(date_to_const_count)
df=df.transpose()
df=df.iloc[:,0]

x=range(0,425)
y_av=detect_anomaly(df,10)
plt.plot(x, df,"k.")
plt.plot(x, y_av,"r")
#xlim(0,1000)
#xlabel("Months since Jan 1749.")
#ylabel("No. of Sun spots")
plt.grid(True)
plt.show()
'''