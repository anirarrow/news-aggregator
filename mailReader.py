import smtplib
import time
import imaplib
import email
from extract_text_from_html import get_text
from pymongo import MongoClient
import pprint
from  bson import objectid
import re

#Connect to media db
client = MongoClient('10.237.26.154', 27017, j=True)
db = client['media-db']

ORG_EMAIL   = "@gmail.com"
FROM_EMAIL  = "act4dnewsapp" + ORG_EMAIL
FROM_PWD    = "change!me"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT   = 993

'''
Method to extract URL of the news article from the body text in Google alert.
'''
def extractURL(body):
    #list of lines in body text
    body_l=body.split('\n')
    #Extract the very first hyperlink from body text (rest are generally not required)
    for line in body_l:
        if 'https://' in line or 'http://' in line:
            url=line.replace('<','')
            url=url.replace('>','')
            break

    return url

# -------------------------------------------------
#
# Utility to read email from Gmail Using Python
#
# ------------------------------------------------

def read_email_from_gmail():
    fout=open("body_text.txt",'w')
    furl=open("url.txt",'w')
    furl2=open("uncrawled_url.txt",'w')
    
    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    mail.login(FROM_EMAIL,FROM_PWD)
    mail.select('inbox')

    type, data = mail.search(None, 'ALL')
    mail_ids = data[0]
    


    id_list = mail_ids.split()   
    first_email_id = int(id_list[0])
    latest_email_id = int(id_list[-1])

    for i in range(latest_email_id,first_email_id, -1):
        typ, data = mail.fetch(str(i), '(RFC822)' )
        #print(typ)
        #print(data)
        for response_part in data:
            #print(response_part)

            if isinstance(response_part, tuple):
                b = email.message_from_string(str(response_part[1]))
                #b = email.message_from_string(a)
                body = ""

                if b.is_multipart():
                    for part in b.walk():
                        ctype = part.get_content_type()
                        cdispo = str(part.get('Content-Disposition'))

                        # skip any text/plain (txt) attachments
                        if ctype == 'text/plain' and 'attachment' not in cdispo:
                            body = part.get_payload(decode=True)  # decode
                            break
                # not multipart - i.e. plain text, no attachments, keeping fingers crossed
                else:
                    body = b.get_payload(decode=True)
                
                #Extract the news link from body text
                url=extractURL(body)
                print("Extracting text for: "+url)
                furl.write(url+'\n')
                try:
                    text=get_text(url)
                except:
                    furl2.write(url+'\n')
                    continue           

                fout.write(text+'\n')
                fout.write("===============================END OF MESSAGE=================================\n")                
    #except:
    #    furl2.write(url+'\n')
    #   print ("Troublensis")

    fout.close()
    furl.close()
    furl2.close()


def filter_body_text(fname):
    fin=open(fname,'r')
    #lines=fin.readlines()
    fout=open('filtered_body_text.txt','w')
    #for i in range(0,len(lines)):
    for line in fin:
        #line=lines[i]
        #line=line.rstrip()
        #Detect the source of the article
        if line.startswith('![') or line.startswith('[![') or line.startswith('[ !['):
            fout.write(line)
            continue
        if '[' in line or ']' in line or '*' in line or '#' in line or line.count('/')>=3\
        or line=='\n' or '.html' in line or '.jpg' in line or '.png' in line or '--' in line\
        or line.count('-')>=3 or line.count('_')>=3 or '|' in line or 'http' in line or\
        line[0]=='+' or line[0]=='/' or line[0]=='\\':
            continue
        if line[len(line)-2]==')' or line[len(line)-2]==':':
            continue
        else:
            fout.write(line)
    fout.close()
    fin.close()

def store_in_mongo(fname):
    fin=open(fname,'r')
    #google_alerts=db.createCollection('google_alerts')
    #print('google_alerts' in db.list_collection_names())
    mycol=db['google_alerts']
    article_text=''
    source=''
    mydicts=[]
    for line in fin:
        line=line.rstrip()
        if 'END OF MESSAGE' not in line and '![' not in line:
            article_text+=line+' '
        elif '![' in line:
            line=line.replace('![','')
            line=line.replace('[','')
            line=line.replace(']','')
            source+=line+', '
        elif 'END OF MESSAGE' in line:
            mydict={'text':article_text,'source':source}
            mydicts.append(mydict)
            article_text=''
            source=''
    print("#articles="+str(len(mydicts)))
    mycol.insert(mydicts)
            
    fin.close()
    print("Mongo insertion done.")
if __name__ == '__main__':
    #read_email_from_gmail()
    #filter_body_text('body_text.txt')
    store_in_mongo('filtered_body_text.txt')