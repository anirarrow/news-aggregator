import urllib2
import html2text
import sys
import requests
reload(sys)
sys.setdefaultencoding('utf-8')
'''
Method to extract actual url from Google alert's html text.
URL redirection happens.
'''
def extract_link(html):
	if "URL=" in html:
		#Detect the spot where URL= is mentioned.
		for i in range(0,len(html)):
			if html[i]=="U" and html[i+1]=="R" and html[i+2]=="L" and html[i+3]=="=":
				break
		j=i+4
		#Detect the spot where .cms is mentioned.
		for x in range(j,len(html)):
			if html[x]=='.' and html[x+1]=='c' and html[x+2]=='m' and html[x+3]=='s':
				end=x+4
				break
			elif html[x]=='.' and html[x+1]=='h' and html[x+2]=='t' and html[x+3]=='m':
				end=x+5
				break
			elif html[x]=='\"':
				end=x+1
				break
		start=j
		
		#Extract URL from the detected spots.
		url=html[start:end]
		return url


def get_text(url):	

	req = urllib2.Request(url)
	req.add_header('Accept','text/html')
	req.add_header('User-Agent','Mozilla/5.0')
	response = urllib2.urlopen(req)
	
	html = response.read()
	print("HTML="+html)
	#Extract redirected URL link from html
	url2=extract_link(html)
	print("URL2="+url2)
	
	req2 = urllib2.Request(url2)
	req2.add_header('Accept','text/html')
	req2.add_header('User-Agent','Mozilla/5.0')
	response2 = urllib2.urlopen(req2)

	html2=response2.read()
	h=html2text.HTML2Text()
	return h.handle(html2)
#if __name__ == '__main__':
#	url='https://www.google.com/url?rct=j&sa=t&url=https://economictimes.indiatimes.com/markets/stocks/news/gst-rate-cut-not-all-that-positive-for-the-realty-stocks-heres-why/articleshow/68151868.cms&ct=ga&cd=CAEYACoUMTAxODY2ODI5MTUzMjM5NTcxMzUyGmNmNWE2Yjc0YzBkMWNmMjM6Y29tOmVuOlVT&usg=AFQjCNGSJZ0ORwX2E8b5TZw7P4BOKV-Q5g'
	#url='https://economictimes.indiatimes.com/markets/stocks/news/gst-rate-cut-not-all-that-positive-for-the-realty-stocks-heres-why/articleshow/68151868.cms&ct=ga&cd=CAEYACoUMTAxODY2ODI5MTUzMjM5NTcxMzUyGmNmNWE2Yjc0YzBkMWNmMjM6Y29tOmVuOlVT&usg=AFQjCNGSJZ0ORwX2E8b5TZw7P4BOKV-Q5g'
	#url="https://economictimes.indiatimes.com/markets/stocks/news/gst-rate-cut-not-all-that-positive-for-the-realty-stocks-heres-why/articleshow/68151868.cms"
#	get_text(url)