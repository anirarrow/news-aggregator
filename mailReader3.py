import poplib
import string, random
import io, rfc822
import logging
import base64

SERVER = "pop.gmail.com"
USER  = "act4dnewsapp@gmail.com"
PASSWORD = "change!me"

#USER  = "wikiban43@gmail.com"
#PASSWORD = "laracameron"

# connect to server
logging.debug('connecting to ' + SERVER)
server = poplib.POP3_SSL(SERVER)
#server = poplib.POP3(SERVER)

# login
logging.debug('logging in')
server.user(USER)
server.pass_(PASSWORD)

'''
Method to extract body text from message containing all sorts of html tags, content-types, etc.
'''
def filterMail(string):
    #List of all lines
    l_lines=string.split('\n')
    #List of required body text lines
    l_body_lines=[]
    for line in l_lines:
        #If the line does not have '------' or other headers, only then store it (in lowercase).
        print("###")
        if 'content-' not in line.lower() and '---' not in line.lower() and '<' not in line.lower() and '>' not in line.lower():
            l_body_lines.append(line.lower())
    return(l_body_lines)


if __name__ == '__main__':
    fout=open("emails2.txt","w")
    # list items on server
    logging.debug('listing emails')
    resp, items, octets = server.list()
    #For each email message
    for i in range(0,len(items)):
        #Download the ith message in the list.
        id, size = string.split(items[i])
        resp, text, octets = server.retr(id)

        # Convert list to Message object.
        text = string.join(text, "\n")
        file1 = io.StringIO(unicode(text))
        message = rfc822.Message(file1)

        # Output message attributes in a file.
        fout.write("From:"+str(message['From']+'\n'))
        fout.write("Sub:"+str(message['Subject']+'\n'))
        fout.write("Date:"+str(message['Date']+'\n'))
        
        #Extract entire message with all html code, text, content-type, etc.
        string=message.fp.read()
        #string=base64.b64decode(string)
        #Extract only the message body (only text) in the form of list of lines.
        l_body_lines=filterMail(string)
        for line in l_body_lines:
            if len(line)>0:
                fout.write(line+'\n')
        fout.write('=======================END OF MESSAGE=========================')
        break
    fout.close()